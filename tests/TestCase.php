<?php

namespace Tests;


class TestCase extends \Orchestra\Testbench\TestCase
{

    protected function setUp(): void
    {
        parent::setUp();

    }
    protected function getPackageProviders($app)
    {
        parent::getPackageProviders($app);
        return [
        'Autodeal\CmTextWhatsappServiceProvider'
        ];
    }
    public function getEnvironmentSetUp($app)
    {
        parent::getEnvironmentSetUp($app);

        $app->setBasePath(realpath(__DIR__ . '/../src'));

        $app['config']->set([
        'cmtext.api_key' => 'a1656f58',
        'cmtext.gateway' => \CMText\Gateways::GLOBAL,
        'cmtext.company_name' => 'AutodealMx'
        ]);
    }
}
