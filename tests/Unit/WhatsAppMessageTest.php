<?php

namespace Tests\Unit;

use Tests\TestCase;
use CMText\TextClient;
use Autodeal\WhatsAppMessage;

class WhatsAppMessageTest extends TestCase
{
  protected $whatsappMessage;

  public function setup(): void
    {
        parent::setup();

        $this->whatsappMessage = new WhatsAppMessage();

    }
  /** @test */
  function gets_the_info_instance_class()
  {

    $this->assertInstanceOf(WhatsAppMessage::class, $this->whatsappMessage);

  }
  /** @test */
  function gets_the_info_phones()
  {
    $this->whatsappMessage->to(['3234234354']);

    $this->assertClassHasAttribute('to', WhatsAppMessage::class);
  }
  /** @test */
  function gets_the_info_template_id()
  {
    $this->whatsappMessage->templateId('3234234354');

    $this->assertClassHasAttribute('templateId', WhatsAppMessage::class);
  }
  /** @test */
  function gets_the_info_template_name()
  {
    $this->whatsappMessage->templateName('prueba');

    $this->assertClassHasAttribute('templateName', WhatsAppMessage::class);
  }
  /** @test */
  function gets_the_info_line_text()
  {
    $this->whatsappMessage->lineText('prueba');

    $this->assertClassHasAttribute('lineText', WhatsAppMessage::class);
  }
  /** @test */
  function gets_the_info_parameters()
  {
    $this->whatsappMessage->parameters(['Hello world']);

    $this->assertClassHasAttribute('parameters', WhatsAppMessage::class);
  }
  /** @test */
  function gets_the_info_button_url()
  {
    $this->whatsappMessage->buttonUrl('http://localhost');

    $this->assertClassHasAttribute('buttonUrl', WhatsAppMessage::class);
  }
  /** @test */
  function send_message_whatsapp_get_status_code_invalid_token()
  {
    $response = $this->whatsappMessage->lineText('Message text')
                            ->to(['1234567834'])
                            ->templateId('d8938f03')
                            ->templateName('prueba')
                            ->parameters(['test autodeal'])
                            ->buttonUrl('http://localhost')
                            ->send();

    $this->assertSame(103,$response->statusCode);
  }
}
