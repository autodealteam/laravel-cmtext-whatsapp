<?php

return [

    /*
    *--------------------------------------------------------------------------
    * CM.com Product token Api Key
    *--------------------------------------------------------------------------
    *
    * This option defines the Api Key to authenticate
    * against the CM.com Messaging gateway api.
    *
    */

    'api_key' => env('PRODUCT_TOKEN_WHATSAPP', ''),

    /*
    *--------------------------------------------------------------------------
    * Gateway * this optional set global gateway
    *--------------------------------------------------------------------------
    *
    * With this setting you can control the gateway that is used. We have
    * gateways around the world, using one other than the default one
    * could help against latency or local regulatory boundaries.
    *
    */

    'gateway' => env('CM_GATEWAY', \CMText\Gateways::GLOBAL),
    /**
     * Name of the company to message
     */
    'company_name' => env('COMPANY_NAME', config('app.name'))

];