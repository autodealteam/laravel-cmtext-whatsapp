<?php

namespace Autodeal;

use CMText\Message;
use CMText\Channels;
use CMText\TextClient;
use CMText\RichContent\Messages\TemplateMessage;
use CMText\RichContent\Templates\Whatsapp\Language;
use CMText\RichContent\Templates\Whatsapp\ComponentBody;
use CMText\RichContent\Templates\Whatsapp\WhatsappTemplate;
use CMText\RichContent\Templates\Whatsapp\ComponentButtonUrl;
use CMText\RichContent\Templates\Whatsapp\ComponentParameterText;

class WhatsAppMessage
{
  protected $textClient;
  protected $from;
  protected $to = [];
  protected $templateId;
  protected $templateName;
  protected $parameters = [];
  protected $buttonUrl;
  protected $lineText;

  public function __construct()
  {
    if (!file_exists(config_path('cmtext.php'))) {
      throw new \Exception(
        sprintf(
          'no cmtext config file was found [%s]',
          'cmtext.php'
        )
      );
    }
    if (empty(config('cmtext.api_key'))) {
      throw new \Exception(
        sprintf(
          'The api_key cannot be empty [%s]',
          config('cmtext.api_key')
        )
      );
    }
    $this->from = config('cmtext.company_name');
    $this->textClient = new TextClient(config('cmtext.api_key'), config('cmtext.gateway'));
  }
  public function to(array $phones): self
  {
    foreach ($phones as $phone) {
      $this->to[] = $this->formatPhoneNumberCM($phone);
    }

    return $this;
  }
  public function templateId(String $templateId): self
  {
    $this->templateId = $templateId;

    return $this;
  }
  public function templateName(String $templateName): self
  {
    $this->templateName = $templateName;

    return $this;
  }
  public function lineText(String $messageText): self
  {
      $this->lineText = $messageText;

      return $this;
  }
  public function parameters(Array $parameters)
  {
      $this->parameters = $parameters;

      return $this;
  }
  public function buttonUrl(String $url = null): self
  {
      $this->buttonUrl = $url;

      return $this;
  }
  public function send()
  {
    if (!$this->from || !count($this->to) || empty($this->templateId) || empty($this->templateName)) {
        throw new \Exception('Whatsapp not correct.');
    }
    $message = new Message($this->lineText, $this->from, $this->to);
    $message
          ->WithChannels([Channels::WHATSAPP])
          ->WithTemplate(
              new TemplateMessage(
                  new WhatsappTemplate(
                      $this->templateId,
                      $this->templateName,
                      new Language('es_MX'),
                      [
                          new ComponentBody(
                              $this->componentParameterText()
                          ),
                          $this->withComponentButton()
                      ]
                  )
              )
            );
      try {
          $result = $this->textClient->send([$message]);

          return $result;
      } catch (\Throwable $th) {
        throw $th;
      }

  }
  protected function componentParameterText()
  {
    if (count($this->parameters) === 0) {
        return [];
    }
    foreach ($this->parameters as $parameter) {
        $componentsText[] = new ComponentParameterText($parameter);
      }
    return $componentsText;
  }
  protected function withComponentButton()
  {
    if (is_null($this->buttonUrl)) {
      return null;
    }
    return new ComponentButtonUrl(0, new ComponentParameterText($this->buttonUrl));
  }
  protected function formatPhoneNumberCM(String $phone)
  {
    $replace = preg_replace('/-+/', '', $phone);

    return "0052{$replace}";
  }
}
