# CM.com Textclient and WhatsApp channel Service Provider

This Project utilizes [Composer](https://getcomposer.org/) to manage its dependencies. So, before install this package, make sure you have Composer installed on your machine.

Run this command:
```php
composer --version
```
WhatsApp channel to send notifications

### Installation

Install package via composer:
```php
composer require autodeal/laravel-cmtext-whatsapp
```
Publish the enclosed config to config/cmtext.php
```php
php artisan vendor:publish --tag=config
```
Add `PRODUCT_TOKEN_WHATSAPP` to the project .env file, the value should be your Api Key product

### Hot to use it

Create a new notification file with next command `php artisan make:notification TestNotification` once these class is created, you must implement the logic you need in each of them.

For example, thinking about the TestNotification class, nice, now we want our notification to send SMS to our users. For that to work we will add our notification to use our new driver, this is done in the via method.
```
<?php

namespace App\Notifications;

use Autodeal\WhatsAppChannel;
use Autodeal\WhatsAppMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PruebaNotification extends Notification
{
    use Queueable;

    public function via($notifiable)
    {
        // So instead of the "default" mail channel.. Lets change or add our notification to notify user with the WhatsAppChannel class.
        // We also need to import our WhatsAppChannel at the top of our file for this to work.
        return [WhatsAppChannel::class];
        // return ['mail', WhatsAppChannel::class]; both channels
    }
}
```
Now we also need to specify what our WhatsAppChannel need to receive to send the whatsapp. Like a telephone number and a message for example.
This is done with a method we call toWhatsapp() instead of the default toMail() in our Notification.

### Code example
```
<?php

namespace App\Notifications;

use Autodeal\WhatsAppChannel;
use Autodeal\WhatsAppMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PruebaNotification extends Notification
{
    use Queueable;
    public function via($notifiable)
    {
        return [WhatsAppChannel::class];
    }
    public function toWhatsapp($notifiable)
    {
        return (new WhatsAppMessage)
                    ->lineText('Message text')
                    ->to(['1239493042'])
                    ->templateId('id_templa_into_cm_platform')
                    ->templateName('test_name_template')
                    ->parameters([
                        'tex'
                    ]);
    }
}
```
### Security

If you find any security issues, please email us at felipe@autodeal.mx or giovanny@autodeal.mx
